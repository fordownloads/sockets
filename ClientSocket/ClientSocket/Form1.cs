﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace ClientSocket
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 2650);
            Socket listenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            
            //отправка ip
            listenSocket.Connect(ipPoint);
            listenSocket.Send(Encoding.Unicode.GetBytes(Dns.GetHostByName(Dns.GetHostName()).AddressList[0].ToString()));
            listenSocket.Disconnect(true);

            //Принимает сообщение
            listenSocket.Bind(ipPoint);
            listenSocket.Listen(10);
            while (true)
            {
                Socket handler = listenSocket.Accept();
                StringBuilder builder = new StringBuilder();
                byte[] data = new byte[2048];

                do builder.Append(Encoding.Unicode.GetString(data, 0, handler.Receive(data)));
                while (handler.Available > 0);

                MessageBox.Show(builder.ToString());

                handler.Send(Encoding.Unicode.GetBytes("ok"));

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
