﻿namespace HackApp
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnStartServer = new System.Windows.Forms.Button();
            this.PCList = new System.Windows.Forms.ListBox();
            this.StatusContainer = new System.Windows.Forms.StatusStrip();
            this.ProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.BtnSend = new System.Windows.Forms.Button();
            this.PortText = new System.Windows.Forms.TextBox();
            this.SplitContainer = new System.Windows.Forms.SplitContainer();
            this.NewIPText = new System.Windows.Forms.TextBox();
            this.PanelServer = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.IPText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnAddServer = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.Journal = new System.Windows.Forms.TextBox();
            this.PanelMsg = new System.Windows.Forms.Panel();
            this.MsgText = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.CurrentIPText = new System.Windows.Forms.Label();
            this.BtnMenu = new System.Windows.Forms.Button();
            this.StatusContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer)).BeginInit();
            this.SplitContainer.Panel1.SuspendLayout();
            this.SplitContainer.Panel2.SuspendLayout();
            this.SplitContainer.SuspendLayout();
            this.PanelServer.SuspendLayout();
            this.PanelMsg.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnStartServer
            // 
            this.BtnStartServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnStartServer.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BtnStartServer.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro;
            this.BtnStartServer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BtnStartServer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.BtnStartServer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnStartServer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnStartServer.Location = new System.Drawing.Point(120, 88);
            this.BtnStartServer.Name = "BtnStartServer";
            this.BtnStartServer.Size = new System.Drawing.Size(72, 24);
            this.BtnStartServer.TabIndex = 6;
            this.BtnStartServer.Text = "Запуск";
            this.BtnStartServer.UseVisualStyleBackColor = false;
            this.BtnStartServer.Click += new System.EventHandler(this.ClickBtnServer);
            // 
            // PCList
            // 
            this.PCList.AllowDrop = true;
            this.PCList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PCList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.PCList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PCList.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PCList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PCList.FormattingEnabled = true;
            this.PCList.IntegralHeight = false;
            this.PCList.ItemHeight = 17;
            this.PCList.Items.AddRange(new object[] {
            "192.168.1.55",
            "192.168.1.57"});
            this.PCList.Location = new System.Drawing.Point(24, 48);
            this.PCList.Margin = new System.Windows.Forms.Padding(8);
            this.PCList.Name = "PCList";
            this.PCList.Size = new System.Drawing.Size(176, 268);
            this.PCList.TabIndex = 3;
            this.PCList.SelectedIndexChanged += new System.EventHandler(this.PCList_SelectedIndexChanged);
            // 
            // StatusContainer
            // 
            this.StatusContainer.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.StatusContainer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProgressBar,
            this.StatusLabel});
            this.StatusContainer.Location = new System.Drawing.Point(0, 557);
            this.StatusContainer.Name = "StatusContainer";
            this.StatusContainer.Size = new System.Drawing.Size(592, 29);
            this.StatusContainer.TabIndex = 4;
            this.StatusContainer.Text = "statusStrip1";
            // 
            // ProgressBar
            // 
            this.ProgressBar.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.ProgressBar.MarqueeAnimationSpeed = 60;
            this.ProgressBar.Maximum = 1;
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(50, 17);
            this.ProgressBar.Step = 1;
            this.ProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.ProgressBar.Value = 1;
            this.ProgressBar.Visible = false;
            // 
            // StatusLabel
            // 
            this.StatusLabel.BackColor = System.Drawing.Color.Transparent;
            this.StatusLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StatusLabel.Margin = new System.Windows.Forms.Padding(8);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(37, 13);
            this.StatusLabel.Text = "Готов";
            // 
            // BtnSend
            // 
            this.BtnSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSend.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BtnSend.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro;
            this.BtnSend.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BtnSend.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.BtnSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSend.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnSend.Location = new System.Drawing.Point(304, 32);
            this.BtnSend.Name = "BtnSend";
            this.BtnSend.Size = new System.Drawing.Size(80, 24);
            this.BtnSend.TabIndex = 2;
            this.BtnSend.Text = "Отправить";
            this.BtnSend.UseVisualStyleBackColor = false;
            this.BtnSend.Click += new System.EventHandler(this.ClickBtnSend);
            // 
            // PortText
            // 
            this.PortText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PortText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.PortText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PortText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PortText.Location = new System.Drawing.Point(72, 74);
            this.PortText.Margin = new System.Windows.Forms.Padding(6);
            this.PortText.Name = "PortText";
            this.PortText.Size = new System.Drawing.Size(120, 13);
            this.PortText.TabIndex = 99;
            this.PortText.Text = "2560";
            // 
            // SplitContainer
            // 
            this.SplitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SplitContainer.BackColor = System.Drawing.Color.LightGray;
            this.SplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.SplitContainer.IsSplitterFixed = true;
            this.SplitContainer.Location = new System.Drawing.Point(0, 73);
            this.SplitContainer.Name = "SplitContainer";
            // 
            // SplitContainer.Panel1
            // 
            this.SplitContainer.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.SplitContainer.Panel1.Controls.Add(this.NewIPText);
            this.SplitContainer.Panel1.Controls.Add(this.PanelServer);
            this.SplitContainer.Panel1.Controls.Add(this.BtnAddServer);
            this.SplitContainer.Panel1.Controls.Add(this.label3);
            this.SplitContainer.Panel1.Controls.Add(this.PCList);
            this.SplitContainer.Panel1MinSize = 200;
            // 
            // SplitContainer.Panel2
            // 
            this.SplitContainer.Panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.SplitContainer.Panel2.Controls.Add(this.Journal);
            this.SplitContainer.Panel2.Controls.Add(this.PanelMsg);
            this.SplitContainer.Panel2MinSize = 0;
            this.SplitContainer.Size = new System.Drawing.Size(592, 484);
            this.SplitContainer.SplitterDistance = 200;
            this.SplitContainer.SplitterWidth = 1;
            this.SplitContainer.TabIndex = 6;
            // 
            // NewIPText
            // 
            this.NewIPText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NewIPText.BackColor = System.Drawing.Color.WhiteSmoke;
            this.NewIPText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NewIPText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.NewIPText.Location = new System.Drawing.Point(24, 328);
            this.NewIPText.Name = "NewIPText";
            this.NewIPText.Size = new System.Drawing.Size(128, 20);
            this.NewIPText.TabIndex = 4;
            // 
            // PanelServer
            // 
            this.PanelServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelServer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.PanelServer.Controls.Add(this.label5);
            this.PanelServer.Controls.Add(this.IPText);
            this.PanelServer.Controls.Add(this.BtnStartServer);
            this.PanelServer.Controls.Add(this.label4);
            this.PanelServer.Controls.Add(this.label2);
            this.PanelServer.Controls.Add(this.PortText);
            this.PanelServer.Location = new System.Drawing.Point(0, 364);
            this.PanelServer.Name = "PanelServer";
            this.PanelServer.Size = new System.Drawing.Size(200, 120);
            this.PanelServer.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(16, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(176, 24);
            this.label5.TabIndex = 13;
            this.label5.Text = "Сервер";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // IPText
            // 
            this.IPText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.IPText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.IPText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.IPText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.IPText.Location = new System.Drawing.Point(72, 52);
            this.IPText.Margin = new System.Windows.Forms.Padding(6);
            this.IPText.Name = "IPText";
            this.IPText.ReadOnly = true;
            this.IPText.Size = new System.Drawing.Size(120, 13);
            this.IPText.TabIndex = 98;
            this.IPText.DoubleClick += new System.EventHandler(this.ClickIPText);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.ForeColor = System.Drawing.Color.Gray;
            this.label4.Location = new System.Drawing.Point(24, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "IP:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.ForeColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(24, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "Порт:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnAddServer
            // 
            this.BtnAddServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnAddServer.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BtnAddServer.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro;
            this.BtnAddServer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BtnAddServer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.BtnAddServer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAddServer.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BtnAddServer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnAddServer.Location = new System.Drawing.Point(160, 328);
            this.BtnAddServer.Name = "BtnAddServer";
            this.BtnAddServer.Size = new System.Drawing.Size(24, 20);
            this.BtnAddServer.TabIndex = 6;
            this.BtnAddServer.Text = "+";
            this.BtnAddServer.UseVisualStyleBackColor = false;
            this.BtnAddServer.Click += new System.EventHandler(this.ClickAddIP);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(16, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(176, 24);
            this.label3.TabIndex = 8;
            this.label3.Text = "Компьютеры";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Journal
            // 
            this.Journal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Journal.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Journal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Journal.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Journal.Location = new System.Drawing.Point(8, 8);
            this.Journal.Multiline = true;
            this.Journal.Name = "Journal";
            this.Journal.ReadOnly = true;
            this.Journal.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.Journal.Size = new System.Drawing.Size(382, 404);
            this.Journal.TabIndex = 5;
            // 
            // PanelMsg
            // 
            this.PanelMsg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelMsg.BackColor = System.Drawing.Color.White;
            this.PanelMsg.Controls.Add(this.BtnSend);
            this.PanelMsg.Controls.Add(this.MsgText);
            this.PanelMsg.Location = new System.Drawing.Point(0, 420);
            this.PanelMsg.Name = "PanelMsg";
            this.PanelMsg.Size = new System.Drawing.Size(404, 64);
            this.PanelMsg.TabIndex = 4;
            // 
            // MsgText
            // 
            this.MsgText.AcceptsReturn = true;
            this.MsgText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MsgText.BackColor = System.Drawing.Color.White;
            this.MsgText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MsgText.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MsgText.Location = new System.Drawing.Point(8, 8);
            this.MsgText.Name = "MsgText";
            this.MsgText.Size = new System.Drawing.Size(382, 22);
            this.MsgText.TabIndex = 1;
            this.MsgText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PressMsgText);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.CurrentIPText);
            this.panel1.Controls.Add(this.BtnMenu);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(592, 72);
            this.panel1.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(56, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(224, 32);
            this.label1.TabIndex = 6;
            this.label1.Text = "Сокеты";
            // 
            // CurrentIPText
            // 
            this.CurrentIPText.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CurrentIPText.ForeColor = System.Drawing.Color.Gray;
            this.CurrentIPText.Location = new System.Drawing.Point(58, 38);
            this.CurrentIPText.Name = "CurrentIPText";
            this.CurrentIPText.Size = new System.Drawing.Size(224, 16);
            this.CurrentIPText.TabIndex = 8;
            this.CurrentIPText.Text = "192.168.1.55";
            this.CurrentIPText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BtnMenu
            // 
            this.BtnMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BtnMenu.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro;
            this.BtnMenu.FlatAppearance.BorderSize = 0;
            this.BtnMenu.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gainsboro;
            this.BtnMenu.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.BtnMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnMenu.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BtnMenu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnMenu.Location = new System.Drawing.Point(0, 0);
            this.BtnMenu.Margin = new System.Windows.Forms.Padding(0);
            this.BtnMenu.Name = "BtnMenu";
            this.BtnMenu.Size = new System.Drawing.Size(48, 72);
            this.BtnMenu.TabIndex = 7;
            this.BtnMenu.Text = " ≡";
            this.BtnMenu.UseVisualStyleBackColor = false;
            this.BtnMenu.Click += new System.EventHandler(this.ClickBtnMenu);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(592, 586);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.SplitContainer);
            this.Controls.Add(this.StatusContainer);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "Form1";
            this.Text = "Сокеты";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CloseForm);
            this.StatusContainer.ResumeLayout(false);
            this.StatusContainer.PerformLayout();
            this.SplitContainer.Panel1.ResumeLayout(false);
            this.SplitContainer.Panel1.PerformLayout();
            this.SplitContainer.Panel2.ResumeLayout(false);
            this.SplitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer)).EndInit();
            this.SplitContainer.ResumeLayout(false);
            this.PanelServer.ResumeLayout(false);
            this.PanelServer.PerformLayout();
            this.PanelMsg.ResumeLayout(false);
            this.PanelMsg.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnStartServer;
        private System.Windows.Forms.ListBox PCList;
        private System.Windows.Forms.StatusStrip StatusContainer;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.ToolStripProgressBar ProgressBar;
        private System.Windows.Forms.Button BtnSend;
        private System.Windows.Forms.TextBox PortText;
        private System.Windows.Forms.SplitContainer SplitContainer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox MsgText;
        private System.Windows.Forms.Panel PanelMsg;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox IPText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel PanelServer;
        private System.Windows.Forms.TextBox NewIPText;
        private System.Windows.Forms.TextBox Journal;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button BtnAddServer;
        private System.Windows.Forms.Button BtnMenu;
        private System.Windows.Forms.Label CurrentIPText;
    }
}

