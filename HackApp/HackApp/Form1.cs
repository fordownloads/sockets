﻿using System;
using System.Net.Sockets;
using System.Net;
using System.Windows.Forms;
using System.Text;
using System.Threading;

namespace HackApp
{
    public partial class Form1 : Form
    {
        string IPetc = "";
        public Form1()
        {
            InitializeComponent();
            PCList.SelectedIndex = 0;

            foreach (var ip in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    if (!ip.ToString().Contains(".56.") && IPText.Text == "")
                        IPText.Text = ip.ToString();
                    IPetc += ip.ToString() + Environment.NewLine;
                }
        }

        Socket InitSocket() => new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        void Status (string text) => StatusLabel.Text = text;
        void Log    (string text) => Journal.Text += text + Environment.NewLine;

        void ClickAddIP (object s, EventArgs e)            => AddIP(NewIPText.Text);
        void CloseForm  (object s, FormClosingEventArgs e) => Environment.Exit(0);
        void ClickBtnMenu(object sender, EventArgs e) => SplitContainer.Panel1Collapsed = !SplitContainer.Panel1Collapsed;

        void Error(Exception e)
        {
            MessageBox.Show(e.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            Status("Ошибка");
        }

        void PressMsgText (object s, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                BtnSend.PerformClick();
        }

        void AddIP(string ip)
        {
            if (!PCList.Items.Contains(ip))
            {
                PCList.Items.Add(ip);
                Status("Добавлен IP: " + ip);
            }
        }

        void ClickBtnSend(object s, EventArgs e)
        {
            SplitContainer.Enabled = false;
            ProgressBar.Visible = true;
            Status("Отправка...");
            new Thread(new ThreadStart(SocketSendThread)).Start();
        }

        void ClickBtnServer(object s, EventArgs e)
        {
            BtnStartServer.Enabled = false;
            BtnStartServer.Text = "Запущен";
            new Thread(new ThreadStart(SocketReceive)).Start();
        }

        public void SocketReceive()
        {
            try
            {
                using (Socket socket = InitSocket())
                {
                    socket.Bind(new IPEndPoint(IPAddress.Any, Convert.ToInt32(PortText.Text)));
                    socket.Listen(10);

                    Log("ПК принимает сообщения на порт " + PortText.Text);

                    while (true)
                    {
                        using (Socket handler = socket.Accept())
                        {
                            StringBuilder builder = new StringBuilder();
                            byte[] data = new byte[2048];

                            do builder.Append(Encoding.Unicode.GetString(data, 0, handler.Receive(data)));
                            while (handler.Available > 0);

                            string address = handler.LocalEndPoint.ToString().Split(':')[0];
                            AddIP(address);

                            Log(address + ": " + builder.ToString());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Error(e);
                BtnStartServer.Enabled = true;
                BtnStartServer.Text = "Запуск";
            }
        }

        public void SocketSendThread()
        {
            try {
                using (Socket socket = InitSocket())
                {
                    socket.Connect(new IPEndPoint(IPAddress.Parse(PCList.Text), Convert.ToInt32(PortText.Text)));
                    socket.Send(Encoding.Unicode.GetBytes(MsgText.Text));
                }

                Log(">> " + PCList.Text + ": " + MsgText.Text);
                Status("Отправлено");
            }
            catch (Exception e)
            {
                Error(e);
            }

            SplitContainer.Enabled = true;
            ProgressBar.Visible = false;
        }

        private void PCList_SelectedIndexChanged(object sender, EventArgs e) => CurrentIPText.Text = PCList.Text;

        private void ClickIPText(object sender, EventArgs e)
        {
            MessageBox.Show("Найденные IP:\n" + IPetc, "Доступные IP", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
